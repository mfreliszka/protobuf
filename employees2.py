import employees_pb2

employees = employees_pb2.Employees()

michal = employees.employees.add()
michal.id =1001
michal.name = "Michal"
michal.salary = 3500

tomek = employees.employees.add()
tomek.id =1002
tomek.name = "Tomek"
tomek.salary = 5000

maciek = employees.employees.add()
maciek.id =1003
maciek.name = "Maciek"
maciek.salary = 2500

with open('employeesbinary', 'wb') as f:
    f.write(employees.SerializeToString())
    print(employees.SerializeToString())

with open('employeesbinary', 'rb') as f:
    empBytes = employees_pb2.Employees()
    empBytes.ParseFromString(f.read())
    print(empBytes)