import json

employees = []

michal = {
    "name":"Michal",
    "salary": 2000,
    "id":1
}

tomek = {
    "name":"Tomek",
    "salary": 3000,
    "id":2
}
maciek = {
    "name":"Maciek",
    "salary": 1000,
    "id":3
}

employees.append(michal)
employees.append(tomek)
employees.append(maciek)

jsonString = json.dumps(employees)
jsonFile = open("jsondata.json", "w")
jsonFile.write(jsonString)
jsonFile.close()